'''
Basic flask server used to store JavaScript that is loaded into
browser to enable communication between game and the programm.
JavaScript: ./static/botScript.js
'''
from flask import Flask

APP = Flask(__name__)

if __name__ == '__main__':
    Flask.run(APP)
