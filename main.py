'''
Main module responsible for control and
execution of other modules and program loop.
'''

from multiprocessing import Process, Queue
import time
import asyncio
import numpy
import mss
import websockets
import basic
import sys


def wsserver(message_queue):
    '''
    Start websocket server responsible for
    receiving game state from the browser.
    '''

    # pylint: disable=unused-argument
    async def echo(websocket, path):
        async for message in websocket:
            nonlocal message_queue
            message_queue.put(message)

    asyncio.get_event_loop().run_until_complete(
        websockets.serve(echo, 'localhost', 5005,
                         read_limit=2**50, max_size=2**50))

    asyncio.get_event_loop().run_forever()


def get_screenshot():
    '''Returns screenshot of full screen as a numpy array.'''
    with mss.mss() as sct:
        return numpy.array(sct.grab(sct.monitors[1]))


def interpret_output(controls):
    '''Interpret neural network output and return directions.'''

    # create arrays representing plots that describe probability of
    # corresponding controls:
    # controls[0][0] - probability of going left
    # controls[0][1] - probability of going straight
    # controls[0][2] - probability of going right
    #
    # controls[0][3] - probability of not applying boost
    # controls[0][3] - probability of applying boost

    direction_plot = [0, controls[0][0], controls[0][0] + controls[0][1],
                      controls[0][0] + controls[0][1] + controls[0][2]]

    boost_plot = [0, controls[0][3], controls[0][3] + controls[0][4]]

    direction_randomization = numpy.random.rand() * sum(direction_plot)
    boost_randomization = numpy.random.rand() * sum(boost_plot)

    direction = 0
    boost = 0

    # randomize controls giving the probability from plots
    if direction_plot[0] <= direction_randomization <= direction_plot[1]:
        direction = -1
    elif direction_plot[1] < direction_randomization <= direction_plot[2]:
        direction = 0
    elif direction_plot[2] < direction_randomization <= direction_plot[3]:
        direction = 1

    elif boost_randomization > boost_plot[1]:
        boost = 1

    return direction, boost


def execute_controls(direction, boost):
    '''Execute controls.'''
    if direction == -1:
        basic.go_left()
    if direction == 0:
        basic.go_straight()
    if direction == 1:
        basic.go_right()
    basic.boost(boost)


class game_state:
    '''Class describing game state at different points in time.'''
    # pylint: disable=too-many-arguments
    def __init__(self, timer, message, input_data, hidden_state,
                 controls, direction, boost):
        self.timer = timer
        self.message = message
        self.input_data = input_data
        self.hidden_state = hidden_state
        self.controls_probability = controls
        self.direction = direction
        self.boost = boost
        self.dscore_dtime = 0

    def add_derivative(self, ds_dt):
        self.dscore_dtime = ds_dt


def linear_interpolation(p_1, p_2, t):
    '''
    Performs linear interpolation between p_1
    and p_2 at the param t.
    '''
    return (p_1[1]*(p_2[0]-t)+p_2[1]*(t-p_1[0]))/(p_2[0]-p_1[0])


def return_target_decision(output, direction, boost, ds_dt):
    '''Given the states in previous times,
    returns what controls would be more desirable in past situatuions'''
    target = output.numpy()
    reinforcement = 1 + 0.01 * ds_dt
    discouragement = 1 - 0.01 * ds_dt

    if ds_dt > 0:
        if direction == -1:
            target[0][0] = target[0][0] * (reinforcement - 0.05)
            target[0][1] = target[0][1] * (discouragement + 0.05)
            target[0][2] = output[0][2] * (discouragement + 0.05)
        elif direction == 0:
            target[0][0] = target[0][0] * (discouragement + 0.05)
            target[0][1] = target[0][1] * (reinforcement - 0.05)
            target[0][2] = target[0][2] * (discouragement + 0.05)
        elif direction == 1:
            target[0][0] = target[0][0] * (discouragement + 0.05)
            target[0][1] = target[0][1] * (discouragement + 0.05)
            target[0][2] = target[0][2] * (reinforcement - 0.05)

        if boost == 1:
            target[0][3] = target[0][3] * reinforcement
            target[0][4] = target[0][4] * discouragement

        if boost == 0:
            target[0][3] = target[0][3] * discouragement
            target[0][4] = target[0][4] * reinforcement

    else:
        if boost == 1:
            target[0][3] = target[0][3] * discouragement
            target[0][4] = target[0][4] * reinforcement

    for i in range(0, 5):
        if target[0][i] == 0:
            target[0][i] = 0.1

    return target


def prepare_training_data(states):
    inputs = []
    hidden_states = []
    targets = []

    for state in states:
        target = return_target_decision(state.controls_probability,
                                        state.direction, state.boost,
                                        state.dscore_dtime)[0]

        inputs.append(state.input_data)
        hidden_states.append(state.hidden_state)
        targets.append(target)

    return inputs, hidden_states, targets


def training_loop(state_queue, mirror_net_queue):
    '''Loop for neural network training on the go.'''
    import nn

    net_mirror = nn.RecurrentNeuralNetwork()
    state_history = []

    first_pass = True
    last_score = 0


    while True:
        if not state_queue.empty():
            # get last state from queue
            state = state_queue.get()

            if state == 'dead':
                # if bot lost the game let it go through all the states in queue
                # and then send 'done' message to the process responsible for conntrols
                # and reset remaining values in this process
                mirror_net_queue.put('done')
                last_score = 0
                state_history.clear()

            else:
                state_history.append(state)

                if not first_pass:

                    # every time a score changes use data from the previous time it changed
                    # until now and use it for training<
                    if last_score != int(state_history[-1].message) and len(state_history) >= 2:
                        # prepare training batch
                        last_score = int(state_history[-1].message)

                        # calculate derivative after gaining points
                        delta_score = float(state_history[-1].message) - float(state_history[-2].message)
                        delta_time = state_history[-1].timer - state_history[-2].timer
                        
                        # interpolate derivaties for previous states
                        point_1 = (state_history[0].timer, 0)
                        point_2 = (state_history[-1].timer, delta_score/delta_time)

                        for state in state_history:
                            state.add_derivative(linear_interpolation(point_1, point_2, state.timer)

                        # prepare training data
                        inputs, hidden_states, targets = prepare_training_data(state_history)
                        state_history.clear()
                        net_mirror.train_network(inputs, hidden_states, targets)
                        
                        # for every ten updates of local weights put one into queue for pick up by other process
                        if trainings >= 10:
                            if not mirror_net_queue.empty():
                                mirror_net_queue.clear()
                            mirror_net_queue.put(net_mirror.variables)
                            trainings = 0

                else:
                    # first pass is used to synchronise variables between
                    # neural network used for learning and network used for controls
                    net_mirror.variables = state
                    state_history.pop(0)
                    first_pass = False
                

def game_state_loop(message_queue, state_queue, mirror_net_queue):
    '''
    Intended to run as separate process to allow
    parallel execution with websocket server and training process
    '''

    import nn

    net = nn.RecurrentNeuralNetwork()
    # at first put current variables into queue
    # to sunchronise neural network in training process.

    state_queue.put(net.variables)
    message = ''
    timer = 0
    last_game_state = None

    while True:
        last_time = time.time()
        if not message_queue.empty():
            message = message_queue.get()
            if message == 'dead':
                # punish bot for losing with the equivalent of losing 50 points
                last_game_state.message = str(int(last_game_state.message) - 50)
                last_game_state.timer = timer + 0.3
                state_queue.put(last_game_state)
                state_queue.put('dead')

                while True:
                    # wait for message 'done' indicating that the other process
                    # finished training neural network, updating weights in the meantime
                    if not mirror_net_queue.empty():
                        mirror = mirror_net_queue.get()
                        if mirror == 'done':
                            # if training for this gameplay finished restart game
                            # and continue playing
                            timer = 0
                            basic.restart_game()
                            break
                        else:
                            net.variables = mirror

        if message not in ('dead', ''):
            controls, hidden_state, data = net.return_controls(get_screenshot())
            direction, boost = interpret_output(controls)
            execute_controls(direction, boost)
            timer += time.time() - last_time
            last_game_state = game_state(timer, message, data, hidden_state, controls, direction, boost)
            state_queue.put(game_state(
                timer, message, data, hidden_state, controls, direction, boost))

if __name__ == '__main__':
    MESSAGE_QUEUE = Queue()     # queue containing messages received from browser during gameplay
    STATE_QUEUE = Queue()       # queue containing game states and neural network parameters at given points in time
    MIRROR_NET_QUEUE = Queue()  # queue containing updated neural weights and biases

    # process for running websocket server, which has access to message queue
    # server receives messages from JavaScript file running in browser and
    # puts it into message queue, so that it can be read from game state loop
    WSSERVER_PROCESS = Process(target=wsserver, args=(MESSAGE_QUEUE,))

    # process in which main game state loop runs, it is used to execute controls,
    # gather game states in next periods of time and put them into state queue,
    # for training process
    # to pick up and interpret and train neural network.
    # it also reads elements from mirror net queue to apply updated weights to
    # its local neural network
    # model.
    STATE_PROCESS = Process(target=game_state_loop, args=(MESSAGE_QUEUE, STATE_QUEUE, MIRROR_NET_QUEUE,))

    # gets states from state queue, interprets them, trains neural network
    # model and puts updated weights into mirror net queue.
    TRAINING_PROCESS = Process(target=training_loop, args=(STATE_QUEUE, MIRROR_NET_QUEUE,))

    WSSERVER_PROCESS.start()
    STATE_PROCESS.start()
    TRAINING_PROCESS.start()

    time.sleep(2)

    basic.initialize_game()

    WSSERVER_PROCESS.join()
    STATE_PROCESS.join()
    TRAINING_PROCESS.join()
