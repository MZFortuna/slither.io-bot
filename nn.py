'''Neural network used to return output based on image input.'''

import tensorflow as tf
import cv2

# Enabling process to allocate 30% of gpu memory
# so that it can simultaneusly perform forward passes and
# learning using backpropagation on different processes

config = tf.ConfigProto()
gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.3)
tf.enable_eager_execution(config=tf.ConfigProto(gpu_options=gpu_options))


class RecurrentNeuralNetwork():
    def __init__(self):
        self.image_size_x = 128
        self.image_size_y = 128

        self.n_inputs = self.image_size_x * self.image_size_y
        self.n_outputs = 5
        self.dropout = 0.75

        # WEIGHTS
        # w_c1, w_c2 - weights for convolutional layers
        # w_d1 - weights for dense layers
        # w_h - weight for hidden state layer
        # w_o - weight for output layer

        self.w_c1 = tf.Variable(tf.random_normal([5, 5, 1, 32]))
        self.w_c2 = tf.Variable(tf.random_normal([5, 5, 32, 64]))
        self.w_d1 = tf.Variable(tf.random_normal([32*32*64, 1024]))
        self.w_h = tf.Variable(tf.random_normal([1024]))
        self.w_o = tf.Variable(tf.random_normal([1024, self.n_outputs]))

        # BIASES
        # b_c1, b_c2 - biases for confolutional layers
        # b_d1 - biases for dense layers
        # b_o - bias for output layer

        self.b_c1 = tf.Variable(tf.zeros([32]))
        self.b_c2 = tf.Variable(tf.zeros([64]))
        self.b_d1 = tf.Variable(tf.zeros([1024]))
        self.b_o = tf.Variable(tf.zeros([self.n_outputs]))

        # HIDDEN STATE

        self.hidden_state = tf.Variable(tf.zeros([1024]))

        # store variabless in array for simpler access

        self.variables = [self.w_c1, self.w_c2, self.w_d1, self.w_h, self.w_o,
                          self.b_c1, self.b_c2, self.b_d1, self.b_o]

    def conv2d(self, input_data, W, b, strides=1):
        input_data = tf.nn.conv2d(input_data, W, strides=[1, strides, strides, 1], padding='SAME')
        input_data = tf.nn.bias_add(input_data, b)
        return tf.nn.softmax(input_data)

    def mainput_datapool2d(self, input_data, k=2):
        return tf.nn.max_pool2d(input_data, ksize=[1, k, k, 1], strides=[1, k, k, 1], padding='SAME')

    def conv_net(self, input_data, hidden_state, variables, dropout):
        input_data = tf.reshape(input_data, shape=[-1, self.image_size_x, self.image_size_y, 1])

        # normalize input, initially R, G, B is between 0 and 255
        input_data = tf.divide(input_data, 255)

        # first convolution pass
        conv1 = self.conv2d(input_data, variables[0], variables[5])
        conv1 = self.maxpool2d(conv1)

        # second convolution pass
        conv2 = self.conv2d(conv1, variables[1], variables[6])
        conv2 = self.maxpool2d(conv2)

        # fully connected layer pass
        fc1 = tf.reshape(conv2, [-1, variables[2].get_shape().as_list()[0]])
        hiddent_activation = tf.multiply(hidden_state, variables[3])
        fc1 = tf.add(tf.matmul(fc1, variables[2]), hiddent_activation)
        fc1 = tf.add(fc1, variables[7])
        fc1 = tf.nn.softmax(fc1)
        fc1 = tf.nn.dropout(fc1, dropout)

        # output layer activation
        out = tf.add(tf.matmul(fc1, variables[4]), variables[8])

        return tf.nn.softmax(out), fc1

    def return_controls(self, image):
        '''
        Returns controls probabilities based on input image,
        hidden state values and input data converted to tensor.
        '''

        # convert image to grayscale, resize image and convert image to tensor
        img = cv2.resize(cv2.cvtColor(image, cv2.COLOR_BGR2GRAY),
                         (self.image_size_x, self.image_size_y), cv2.INTER_NEAREST)
        data = tf.convert_to_tensor(img, dtype=tf.float32)
        output, hidden_state = self.conv_net(data, self.hidden_state, self.variables, self.dropout)
        return output, hidden_state, data

    def train_network(self, inputs, hidden_states, targets):
        '''
        Method responsible for training of the neural network, it uses stochastic gradient descent.
        '''
        optimizer = tf.compat.v1.train.AdamOptimizer()

        for (data, hidden_states, target) in zip(inputs, hidden_states, tarets):
            with tf.GradientTape() as tape:
                loss_values = []
                output = self.conv_net(data, hidden_state, self.variables, self.dropout)[0]
                target = target.reshape((1, 5))
                loss_values.append(tf.compat.v1.losses.mean_squared_error(target, output))

                grads = tape.gradient(loss_values, self.variables)
                optimizer.apply_gradients(zip(grads, self.variables))
