# Slitherio bot
Bot that uses Tensorflow Eager Execution and reinforcement learning to improve itself in playing online game (slither.io).

## Update
### 1. Current task
Currently working on making learning process faster.

### 2. Elements to be added
1. Saving and loading of neural network model from the disk.
2. Killing all processes realted to the bot when controlling process is interrupted.

## Getting Started
### Preparing environment
Bot was tested on Windows 10 operating system. It needs Google Chrome browser installed. In Google Chrome make sure that when opening developer options window (ctr+shift+i) deafault opened window is JavaScript console.

1. Install pywin32 python library:
```
pip install pywin32
```

2. Install libraries from requirements.txt file:
```
pip install -r requirements.txt
```
### Starting program
1. Start flask server:
```
./server.py
```
2. Run main file:
```
python ./main.py
```

## Contents of files
1. basic.py  - functions responsible for controlling game (start, reset, turn left/right)
2. main.py   - main file used to start modules in its processes, main controlling file.
3. nn.py     - contains neural network model and methods used for forward pass and backpropagation.
4. server.py - simple flask server used to serve website(./templates/index.html) with JavaScript file(./static/botScript.js) to be uploaded to browser.
5. botScript.js - file containing browser side logic to communicate with the server.
6. index.html - html file that serves JavaScript file from flask server.