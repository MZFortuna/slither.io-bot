'''
Module containing basic functions for controlling game.
'''

import os
import time
import keyboard
import win32api
import win32con

def click(x_position, y_position):
    '''Click mouse at position (x_position, y_position)'''
    win32api.SetCursorPos((x_position, y_position))
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, x_position, y_position, 0, 0)
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, x_position, y_position, 0, 0)

def move_mouse(x_position, y_position):
    '''Move mouse to (x_position, y_position)'''
    win32api.SetCursorPos((x_position, y_position))

def open_browser_and_inject_js():
    js_code = ('var script = document.createElement("script");'
               'script.type = "text/javascript";'
               'script.src = "http://localhost:5000/static/botScript.js";'
               'document.body.appendChild(script);')

    # pylint: disable=anomalous-backslash-in-string
    run_game_command = '"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" slither.io'

    os.system(run_game_command)
    time.sleep(5)
    keyboard.send('ctrl+shift+i')
    time.sleep(1)
    keyboard.write(js_code)
    keyboard.send('enter')
    time.sleep(1)
    keyboard.send('ctrl+shift+i')
    time.sleep(1)

def go_left():
    '''Controls snake to the left.'''
    keyboard.press('left')
    keyboard.release('right')

def go_right():
    '''Controls snake to the right'''
    keyboard.release('left')
    keyboard.press('right')

def go_straight():
    '''Controls snake straight.'''
    keyboard.release('left')
    keyboard.release('right')

def boost(boost_state):
    '''Turns boost on or off (states: 0 or 1)'''
    if boost_state == 1:
        keyboard.press('up')
    else:
        keyboard.release('up')

def start_game():
    click(500, 500)
    keyboard.send('enter')
    move_mouse(960, 540)
    keyboard.send('F11')

def kill_game():
    keyboard.send('F11')
    time.sleep(1)
    keyboard.send('ctrl+w')
    time.sleep(1)

def initialize_game():
    open_browser_and_inject_js()
    start_game()

def restart_game():
    kill_game()
    initialize_game()
