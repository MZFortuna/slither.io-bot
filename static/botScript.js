// File loaded into browser to send game state to the server

function InputNickname(){ 
    // Input nickname into player nickname field
    document.getElementById('nick').value = 'SlitherBot';
}

function HidePointer(){
    document.body.style.cursor = 'none';
}

function ReturnCurrentScore(){
    return document.body.children[17].children[0].children[1].innerText;
}

function SendGameStateToTheServer(){
    socket.send(ReturnCurrentScore());

    if (document.getElementById("lastscore").children.length != 0){
        clearInterval(interval);
        socket.send('dead');
    }
}

function WaitForGameComponentsToLoad(){
    if (document.body.children[17].children[0]){
        interval = setInterval(SendGameStateToTheServer, 100);
        clearInterval(initInterval);
    }
}

window.onload = InputNickname(), HidePointer();
let interval;
let socket = new WebSocket('ws://localhost:5005');
let initInterval = setInterval(WaitForGameComponentsToLoad, 50);